from django.views.generic import ListView,DetailView
from django import forms
from core.models import Movie,Person,Vote
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.contrib.auth.mixins import (LoginRequiredMixin)
from django.shortcuts import redirect
from django.views.generic import (CreateView,UpdateView)
from django.core.exceptions import PermissionDenied

class MovieList(ListView):
    model = Movie
    paginate_by = 5

class MovieDetail(DetailView):
    queryset = (Movie.objects.all_with_prefetch_persons())

    def get_context_data(self,**kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            vote = None
            vote_form_url = ''
            vote = Vote.objects.get_vote_or_blank(movie=self.object,user=self.request.user)
            if vote.id:
                vote_form_url = reverse('core:UpdateVote',kwargs={'movie_id':vote.movie.id,'pk':vote.id})
            else:
                vote_form_url = reverse('core:CreateVote',kwargs={'movie_id':self.object.id})
            vote_form = VoteForm(instance=vote)
            ctx['vote_form'] = vote_form
            ctx['vote_form_url'] = vote_form_url
        else:   
            ctx['login_url'] = reverse('user:login')
        return ctx           

class PersonDetail(DetailView):
    queryset = Person.objects.all_with_prefetch_movies()

class VoteForm(forms.ModelForm):
    user = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=get_user_model().objects.all(),
        disabled=True
    )
    movie = forms.ModelChoiceField(
        widget = forms.HiddenInput,
        queryset=Movie.objects.all(),
        disabled=True
    )
    value=forms.ChoiceField(
        label='Vote',
        widget = forms.RadioSelect,
        choices=Vote.VALUE_CHOICES
    )
    class Meta:
        model = Vote
        fields = ('value','user','movie')

class CreateVote(LoginRequiredMixin,CreateView):
    form_class = VoteForm

    def get_initial(self):
        initial = super().get_initial()
        initial['user'] = self.request.user.id
        initial['movie'] = self.kwargs['movie_id']
        return initial
    
    def get_success_url(self):
        movie_id = self.object.movie.id
        return reverse('core:MovieDetail',kwargs={'pk':movie_id})

    def render_to_response(self,context,**response_kwargs):
        movie_id = context['object'].id
        movie_detail_url = reverse('core:MovieDetail',kwargs={'pk':movie_id})
        return redirect(to=movie_detail_url)

class UpdateVote(LoginRequiredMixin,UpdateView):
    form_class = VoteForm
    queryset = Vote.objects.all()

    def get_object(self,queryset=None):
        vote = super().get_object(queryset=queryset)
        user = self.request.user
        if vote.user != user:
            raise PermissionDenied('cannot change another user\'s vote')
        return vote
    
    def get_success_url(self):
        movie_id = self.object.movie_id
        return reverse('core:MovieDetail',kwargs={'pk':movie_id})

    def render_to_response(self,context,**response_kwargs):
        movie_id = context['object'].id
        movie_detail_url = reverse('core:MovieDetail',kwargs={'pk':movie_id})
        return redirect(to=movie_detail_url)